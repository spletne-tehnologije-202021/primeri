const http = require('http');
const fs = require('fs');
const PORT = process.env.PORT || 5000;

http.createServer((req, res) => {
    const fileS = fs.createReadStream('trailer.mp4');

    res.writeHead(200, { 'Content-type': 'video/mp4' });
    fileS.pipe(res);

}).listen(PORT, () => {
    console.log(`Strežnik posluša na http://localhost:${PORT}`);
})