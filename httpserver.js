const http = require('http');
const fs = require('fs');

http.createServer((req, res) => {
    res.writeHead(200, { 'Content-type': 'text/html' });
    res.end('<html><head><meta charset=utf-8><title>Prvi primer</title></head><body><h1>Hej!</h1><p>To je naša prva Node.js http aplikacija</p></body></html>')
}).listen(3000, '127.0.0.1', () => {
    console.log('Strežnik posluša na http://localhost:3000');
});