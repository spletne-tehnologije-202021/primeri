const http = require('http');
const path = require('path');
const fs = require('fs');

const PORT = process.env.PORT || 5000;

const avtomobili = [{
        id: 1,
        znamka: 'Audi',
        model: 'A4',
        letnik: 2018,
        cena: 33000
    },
    {
        id: 2,
        znamka: 'Mazda',
        model: 'CX5',
        letnik: 2018,
        cena: 23000
    },
    {
        id: 3,
        znamka: 'Volvo',
        model: 'XC90',
        letnik: 2018,
        cena: 53000
    },
    {
        id: 4,
        znamka: 'BMW',
        model: 'X3',
        letnik: 2018,
        cena: 33000
    }
];

http.createServer((req, res) => {

    const url = (req.url === '/') ? 'index.html' : req.url;
    const filePath = path.join(__dirname, 'public', url);
    console.log(filePath);

    let contentType = 'text/html';

    contentType = getContentType(path.extname(req.url));

    if (url === '/api/avtomobili') {
        res.writeHead(200, { 'Content-type': 'application/json' });
        res.end(JSON.stringify(avtomobili));
    } else {

        fs.readFile(filePath, (err, data) => {

            if (err) {
                if (err.code === 'ENOENT') {
                    res.writeHead(200, { 'Content-type': 'text/html' });

                    fs.readFile(path.join(__dirname, 'public', 'notfound.html'), (err1, dat) => {
                        res.end(dat);
                    });
                } else {
                    res.writeHead(500, { 'Content-type': 'text/html' });
                    res.end({ msg: `Server error: ${err.code}` });
                }
            } else {
                res.writeHead(200, { 'Content-type': contentType });
                res.end(data);
            }
        });


    }


}).listen(PORT, function() {
    console.log(`Strežnik posluša na http://localhost:${PORT}`);
})


function getContentType(ext) {
    let contentType = '';

    switch (extType) {
        case '.css':
            contentType = 'text/css';
            break;
        case '.jpg':
            contentType = 'image/jpg';
            break;
        case '.png':
            contentType = 'text/image/png';
            break;
    }
    return contentType;
}