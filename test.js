const http = require('http');
const fs = require('fs');

http.createServer(function(request, response){

    response.writeHead(200, {'Content-type':'video/mp4'});    
    const stream = fs.createReadStream('btrailer.mp4');
    stream.pipe(response);

}).listen(4000, function(){
    console.log('Server is listening on http://localhost:4000');
})